import {
  GET_METRICS,
  METRICS_LOAD_REQUEST,
  START_CHANNEL,
  STOP_CHANNEL
} from "../constants";

export const getMetrics = updateInterval => {
  return {
    type: GET_METRICS,
    payload: updateInterval
  };
};

export const startMetricsFlow = updateInterval => {
  return {
    type: METRICS_LOAD_REQUEST,
    payload: updateInterval
  };
};

export const startChannel = () => ({ type: START_CHANNEL });
export const stopChannel = () => ({ type: STOP_CHANNEL });
