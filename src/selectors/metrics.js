import { createSelector } from 'reselect'

const metricsErrPointer = state => state.metrics.errors
const metricsPointer = state => state.metrics.metrics
const countLoadsPointer = state => state.metrics.countLoads
const metricsFilterPointer = state => state.metrics.filterMetrics
const componentPerfencesPointer = state => state.metrics.componentsPerfences

export default createSelector(
  [
    metricsErrPointer,
    metricsPointer,
    metricsFilterPointer,
    componentPerfencesPointer,
    countLoadsPointer
  ],
  (err, metrcis, shfilter, perf, countL) => {
    /*
        Parse short filter into filter mask
    */

    let filter
    switch (shfilter.timeInterval) {
      case '1h':
        filter = 'last_hour'
        break
      case '1d':
        filter = 'today'
        break
      case '2d':
        filter = 'yesterday'
        break
      case '3d':
        filter = 'last_3days'
        break
      default:
        filter = 'yesterday'
    }

    /*
        Collect metrics
        - Errors, Zeros, Timeouts on filter
        - Errors, Zeros, Timeouts on average
    */

    const {
      data,
      errors_last_3days,
      errors_yesterday,
      errors_last_hour,
      errors_today
    } = metrcis

    let errors = { value: 0, sum: 0, count: 0, avg: 0 }
    let zeroes = { value: 0, sum: 0, count: 0, avg: 0 }
    let timeouts = { value: 0, sum: 0, count: 0, avg: 0 }

    let computedData = { errors, zeroes, timeouts }

    if (data && data[0]) {
      Object.keys(data[0]).map(s => {
        let num = data[0][s] ? data[0][s] : 0

        if (s.includes('errors')) {
          if (s == 'errors_' + filter) {
            errors.value = num
          }
          errors.sum += num
          errors.count++
        }

        if (s.includes('zeroes')) {
          if (s == 'zeroes_' + filter) {
            zeroes.value = num
          }
          zeroes.sum += num
          zeroes.count++
        }

        if (s.includes('timeout')) {
          if (s == 'timeouts_' + filter) {
            timeouts.value = num
          }
          timeouts.sum += num
          timeouts.count++
        }
      })

      Object.keys(computedData).map(s => {
        computedData[s].avg = computedData[s].sum / computedData[s].count
      })

      /*
    Sort data for FlatScale
    */

      let derrors = {
        errors_last_3days,
        errors_yesterday,
        errors_last_hour,
        errors_today
      }

      Object.keys(derrors).map(s => {
        let show = [
          { code: 500, count: 0 },
          { code: 501, count: 0 },
          { code: 502, count: 0 },
          { code: 'other', count: 0 }
        ]

        let showLabels = [500, 501, 502]
        let showedErrors = []
        let otherErrors = { code: 'Other', count: 0 }
        let countErrors = 0, countBlocks = 0

        if (derrors[s] && derrors[s].length > 0) {
          derrors[s].map(x => {
            countErrors += x.count

            show.map(f => {
              if (f.code == x.code) {
                f.count = x.count ? x.count : 0
              }
              if (f.code == 'other') {
                !showLabels.includes(x.code) ? (f.count += x.count) : f.count
              }
              return f
            })
          })

          show = show.map(m => {
            m.width = m.count * 100 / countErrors
            return m
          })

          derrors[s] = show
        } else {
          derrors[s] = show
        }
      })

      return {
        errors,
        metrics: {
          monitorItem: {
            treshold: perf.monitorItem.treshold,
            data: computedData
          },
          flatScale: derrors['errors_' + filter]
        },
        filter: {
          errors: shfilter
        },
        countLoads: countL
      }
    }
    return undefined
  }
)
