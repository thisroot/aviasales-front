import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Container,
  Header,
  Button,
  Grid,
  Loader,
  Segment,
  Label
} from 'semantic-ui-react'

import {
  getMetrics,
  startMetricsFlow,
  startChannel,
  stopChannel
} from '../../actions'
import selector from '../../selectors/metrics'

import { MonitorItem, FlatScale } from './components'

// import styles from "./styles";
import * as styles from './styles.css'

class Home extends Component {
  constructor (props) {
    super(props)

    this.state = {
      ...props,
      filter: { errors: { timeInterval: '2d' } },
      socketActivity: false
    }

    this.handleBtnOnClick = this.handleBtnOnClick.bind(this)
  }

  componentDidUpdate (prevProps) {
    if (prevProps.countLoads != this.props.countLoads) {
      this.setState({ socketActivity: true })
      setTimeout(() => {
        this.setState({ socketActivity: false })
      }, 2000)
    }
  }

  componentDidMount () {
    this.props.startChannel()
  }

  handleBtnOnClick (updateInterval) {
    this.props.getMetrics(updateInterval)
  }

  isButtonActive = interval => {
    return this.props.filter.errors.timeInterval == interval
      ? 'btn-active'
      : 'btn-inactive'
  }

  renderPage = () => {
    // console.log('PROPS=>', this.props)

    const { metrics, filter } = this.props

    const { flatScale, monitorItem } = metrics

    return (
      <Container className={'cont'}>
        <Container className={'head'}>
          <Header className={'title'} as='h2'>
            <div className={'headerText'}>Main metrics</div>
            <div className={'socketIndicator'}>
              <Loader
                active={this.state.socketActivity}
                inline='centered'
                size='small'
              />
            </div>
          </Header>
          <Button.Group basic>
            <Button
              content={'Last hour'}
              onClick={() => this.handleBtnOnClick('1h')}
              className={this.isButtonActive('1h')}
            />

            <Button
              content={'Today'}
              onClick={() => this.handleBtnOnClick('1d')}
              className={this.isButtonActive('1d')}
            />
            <Button
              content={'Yesterday'}
              onClick={() => this.handleBtnOnClick('2d')}
              className={this.isButtonActive('2d')}
            />

            <Button
              content={'Last 3 days'}
              onClick={() => this.handleBtnOnClick('3d')}
              className={this.isButtonActive('3d')}
            />
          </Button.Group>
        </Container>

        <Grid className={'monitor'}>
          <Grid.Row className={'monitorRow'}>
            <MonitorItem
              treshold={monitorItem.treshold}
              name={'errors'}
              data={monitorItem.data.errors}
            />
            <MonitorItem
              treshold={monitorItem.treshold}
              name={'zeroes'}
              data={monitorItem.data.zeroes}
            />
            <MonitorItem
              treshold={monitorItem.treshold}
              name={'timeouts'}
              data={monitorItem.data.timeouts}
            />
          </Grid.Row>
        </Grid>
        <FlatScale data={flatScale} />
        <Grid className={'itemActivityWrapper'} divided={'vertically'}>
          <Grid.Row>
            <Grid.Column className={'leftColumn'}>
              <div className={'iconPlace'}>
                1
              </div>
              <div className={'metricasPlace'}>
                2
              </div>
            </Grid.Column>
            <Grid.Column className={'rightColumn'}>
              <div className={'info-row'}>1</div>
              <div className={'info-row'}>1</div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    )
  }

  render () {
    return (
      <div className='window'>
        {this.props.metrics && this.renderPage()}
        {!this.props.metrics &&
          <div className={'loaderContainer'}>
            <Loader active size='huge' inline='centered' />
          </div>}
      </div>
    )
  }
}

export default connect(state => selector(state), {
  getMetrics,
  startMetricsFlow,
  startChannel,
  stopChannel
})(Home)
