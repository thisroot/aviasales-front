import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'

export default props => {
  const colors = ['yelow', 'purple', 'blue', 'gray']

  return (
    <Grid className={'flatScale'}>
      <Grid.Row className={'rowFlatScale'}>
        {!props.data.every(s => s.count == 0) &&
          <Grid.Column className={'scaleContainer'}>
            {props.data.map((item, index) => {
              return (
                <Grid.Row
                  key={index}
                  style={{ width: item.width + '%' }}
                  className={`scaleItem ${colors[index]}`}
                />
              )
            })}
          </Grid.Column>}
        {props.data.every(s => s.count == 0) &&
          <Grid.Column className={'scaleContainer'}>
            <Grid.Row style={{ width: '100%' }} className={`scaleItem green`} />
          </Grid.Column>}
      </Grid.Row>
      <Grid.Row>
        {props.data.map((item, index) => {
          return (
            <Grid.Column key={index} className={'itemColumn'}>
              <div className={`scaleLableColor ${colors[index]}`} />
              <div className={'scaleLableText'}>
                {item.code}: {item.count}
              </div>
            </Grid.Column>
          )
        })}
      </Grid.Row>
    </Grid>
  )
}
