import React, { Component } from "react";
import { Grid } from "semantic-ui-react";

export default props => {
  const treshold = props.treshold ? props.treshold : [20, 50];

  let status;
  switch (true) {
    case props.data.value >= treshold[1]:
      status = "danger";
      break;
    case props.data.value >= treshold[0]:
      status = "warning";
      break;
    default:
      status = "good";
  }

  return (
    <Grid.Column className={"monitor-item"}>
      <div className={`indicator-round-${status}`} />
      <div className={"metrics-container"}>
        <Grid.Row className={"metric-bold"}>
          {props.name}: {props.data.value.toFixed(2)} %
        </Grid.Row>
        <Grid.Row className={"metric-gray"}>
          avg: {props.data.avg.toFixed(2)} %
        </Grid.Row>
      </div>
    </Grid.Column>
  );
};
