const styles = {
  window: {
    //flex: "0 0 100%",
    backgroundColor: "#E5E5E5"
  },
  container: {
    position: "absolute",
    left: "20.78%",
    right: "20.78%",
    top: "13.06%",
    bottom: "13.06%",
    background: "#FFFFFF",
    boxShadow: "0px 1px 5px rgba(0, 0, 0, 0.15)",
    borderRadius: "10px",
    paddingTop: "40px",
    paddingLeft: "40px",
    paddingRight: "100px",
    paddingBottom: "55px"
  },
  header: {}
};

export default styles;
