import { combineReducers } from "redux";

import metrics from "./metrics";
import socket from "./socket";

export default combineReducers({
  metrics,
  socket
});
