import {
  GET_METRICS,
  METRICS_LOAD_SUCCESS,
  METRICS_LOAD_FAILURE
} from '../constants'

const initialState = {
  errors: false,
  componentsPerfences: {
    monitorItem: {
      treshold: [2, 4]
    }
  },
  metrics: { flatScale: {}, monitorItem: {} },
  filterMetrics: {
    timeInterval: '2d'
  },
  countLoads: 0
}

export default function metrcisReducer (state = initialState, action) {
  switch (action.type) {
    case GET_METRICS:
      return {
        ...state,
        filterMetrics: {
          timeInterval: action.payload
        }
      }
    case METRICS_LOAD_SUCCESS: {
      return {
        ...state,
        countLoads: state.countLoads + 1,
        metrics: action.payload
      }
    }
    case METRICS_LOAD_FAILURE: {
      return {
        ...state,
        errors: action.payload
      }
    }
    default:
      return state
  }
}
