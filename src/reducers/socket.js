import { CHANNEL_ON, CHANNEL_OFF, SERVER_ON, SERVER_OFF } from "../constants";

const initialState = {
  chanelStatus: false,
  serverStatus: false
};

export default function cosketReducer(state = initialState, action) {
  switch (action.type) {
    case CHANNEL_ON:
      return {
        ...state,
        chanelStatus: true
      };
    case CHANNEL_OFF: {
      return {
        ...state,
        chanelStatus: false,
        serverStatus: false
      };
    }
    case SERVER_ON: {
      return {
        ...state,
        serverStatus: true
      };
    }
    case SERVER_OFF: {
      return {
        ...state,
        serverStatus: false
      };
    }
    default:
      return state;
  }
}
