import { all, fork } from "redux-saga/effects";

import { watchGetMetricsSaga } from "./watchers/getMetrics";
import { startStopChannel } from "./watchers/socketWatcher";

export default function* root() {
  yield all([fork(watchGetMetricsSaga), startStopChannel()]);
}
