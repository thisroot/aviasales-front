import _ from "lodash";

import {
  put,
  takeLatest,
  call,
  select,
  race,
  canceled
} from "redux-saga/effects";

import {
  GET_METRICS,
  METRICS_LOAD_FAILURE,
  METRICS_LOAD_SUCCESS,
  METRICS_LOAD_REQUEST
} from "../../constants";

import { fetchMetrics } from "../../lib/api";

function* workerGetMetricsSaga() {
  try {
    const metrics = yield call(fetchMetrics);

    const metricsPointer = state => state.metrics.metrics;
    const oldMetrics = yield select(metricsPointer);

    if (!_.isEqual(metrics, oldMetrics)) {
      yield put({ type: METRICS_LOAD_SUCCESS, payload: metrics });
    }
  } catch (e) {
    yield put({ type: METRICS_LOAD_FAILURE, payload: "error" });
  }
}

export function* watchGetMetricsSaga() {
  yield takeLatest(GET_METRICS, workerGetMetricsSaga);
}
