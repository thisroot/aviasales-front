import {
  SERVER_OFF,
  SERVER_ON,
  CHANNEL_OFF,
  CHANNEL_ON,
  START_CHANNEL,
  STOP_CHANNEL,
  METRICS_LOAD_SUCCESS
} from '../../constants'

import { eventChannel, delay } from 'redux-saga'
import {
  put,
  takeLatest,
  call,
  select,
  race,
  canceled,
  fork,
  take,
  cancelled
} from 'redux-saga/effects'
import { createSelector } from 'reselect'

import { connect, disconnect, reconnect, socket } from '../../modules/socket'
import { startChannel, stopChannel } from '../../actions'

// This is how channel is created
const createSocketChannel = socket =>
  eventChannel(emit => {
    const handler = data => {
      emit(data)
    }
    socket.on('metrics', handler)
    return () => {
      socket.off('metrics', handler)
    }
  })

// connection monitoring sagas
const listenDisconnectSaga = function * () {
  while (true) {
    yield call(disconnect)
    yield put({ type: SERVER_OFF })
  }
}

const listenConnectSaga = function * () {
  while (true) {
    yield call(reconnect)
    yield put({ type: SERVER_ON })
  }
}

// Saga to switch on channel.
const listenServerSaga = function * () {
  try {
    yield put({ type: CHANNEL_ON })

    const { timeout } = yield race({
      connected: call(connect),
      timeout: delay(2000)
    })
    if (timeout) {
      console.log('Timeout')
      yield put({ type: SERVER_OFF })
    }
    const socket = yield call(connect)
    const socketChannel = yield call(createSocketChannel, socket)
    yield fork(listenDisconnectSaga)
    yield fork(listenConnectSaga)
    yield put({ type: SERVER_ON })

    while (true) {
      const payload = yield take(socketChannel)
      yield put({ type: METRICS_LOAD_SUCCESS, payload: JSON.parse(payload) })
    }
  } catch (error) {
    console.log(error)
  } finally {
    if (yield cancelled()) {
      socket.disconnect(true)
      yield put({ type: CHANNEL_OFF })
    }
  }
}

// saga listens for start and stop actions
export const startStopChannel = function * () {
  while (true) {
    yield take(START_CHANNEL)
    yield race({
      task: call(listenServerSaga),
      cancel: take(STOP_CHANNEL)
    })
  }
}
